class Code
  attr_accessor :pegs
  PEGS = {
    R: false, G: false, B: false, Y: false, O: false, P: false
  }.freeze

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string_of_colors)
    pegs = string_of_colors.upcase
    raise 'error' unless pegs.chars.all? { |l| "RGBYOP".include?(l) }
    Code.new(pegs.split(''))
  end

  def self.random
    Code.new(PEGS.keys.shuffle.take(4))
  end

  def [](i)
    @pegs[i]
  end

  def exact_matches(code)
    number_matched = 0
    code.pegs.each_index { |i| number_matched += 1 if code[i] == pegs[i] }
    number_matched
  end

  def near_matches(code)
    near_matches = []
    code.pegs.each_with_index do |peg, i|
      near_matches << peg if code[i] != pegs[i] && pegs.include?(peg)
    end
    near_matches.uniq.length
  end

  def ==(code)
    code.is_a?(Code) && exact_matches(code) == 4 ? true : false
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Guess a code"
    Code.parse($stdin.gets.chomp)
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    puts "You put #{exact_matches} exact matches!!"
    puts "You put #{near_matches} near matches!!"
  end

end
